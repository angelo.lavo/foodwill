from fastapi.testclient import TestClient
from main import app
from queries.donations import DonationQueries

client = TestClient(app)


class EmptyDonationQueries:
    def get_user_donation(self, id):
        return []


def test_get_donation():
    app.dependency_overrides[DonationQueries] = EmptyDonationQueries

    response = client.get("/api/donations/user/userid")
    assert response.status_code == 200
    assert response.json() == {"donations": []}

    app.dependency_overrides = {}
