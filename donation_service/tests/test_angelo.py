from fastapi.testclient import TestClient
from main import app
from queries.donations import DonationQueries

client = TestClient(app)


class EmptyDonationQueries:
    def get_donation(self, id):
        return None


def test_get_donation():
    app.dependency_overrides[DonationQueries] = EmptyDonationQueries

    response = client.get("/api/donations/donationid")
    assert response.status_code == 200
    assert response.json() is None

    app.dependency_overrides = {}
