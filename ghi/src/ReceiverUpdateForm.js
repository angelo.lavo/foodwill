import { useState } from "react";
import { useLogOutMutation, useUpdateUserMutation } from "./store/userApi";
import { useNavigate } from "react-router-dom";

function FormInput(props) {
  const { id, placeholder, labelText, value, onChange, type } = props;

  return (
    <div className="mb-3">
      <label htmlFor={id} className="form-label">
        {labelText}
      </label>

      <input
        value={value}
        onChange={onChange}
        type={type}
        className="form-control"
        id={id}
        placeholder={placeholder}
      />
    </div>
  );
}

function ReceiverUpdate(props) {
  const [logOut] = useLogOutMutation();
  const navigate = useNavigate();
  const [updateUser] = useUpdateUserMutation();
  const [name, setName] = useState(props.userData.account.full_name);
  const [address, setAddress] = useState(props.userData.account.address);
  const [phone_number, setPhoneNumber] = useState(
    props.userData.account.phone_number
  );
  const [operating_hours, setOperatingHours] = useState(
    props.userData.account.operating_hours
  );
  const [dropoff_instr, setDropoffInstructions] = useState(
    props.userData.account.dropoff_instructions
  );

  const handleSubmit = (e) => {
    e.preventDefault();
    let data = {
      full_name: name,
      address: address,
      phone_number: phone_number,
      operating_hours: operating_hours,
      dropoff_instructions: dropoff_instr,
    };
    updateUser(data)
      .unwrap()
      .then((payload) => {
        logOut()
          .unwrap()
          .then((payload) => {
            navigate("/login");
          })
          .catch((error) => console.error("rejected", error));
      })
      .catch((error) => console.error("rejected", error));
  };

  return (
    <div className="container">
      <div className="alert d-none" role="alert" id="submitted">
        <h5 className="alert-heading">Changes submitted</h5>
      </div>

      <form onSubmit={handleSubmit} id="form">
        <FormInput
          id="name"
          placeholder="Full name"
          labelText="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          type="text"
        />

        <FormInput
          id="address"
          placeholder="Address"
          labelText="Address"
          value={address}
          onChange={(e) => setAddress(e.target.value)}
          type="text"
        />

        <FormInput
          id="phone_number"
          placeholder="Phone number"
          labelText="Phone number"
          value={phone_number}
          onChange={(e) => setPhoneNumber(e.target.value)}
          type="text"
        />

        <FormInput
          id="operating_hours"
          placeholder="Operating Hours"
          labelText="Operating Hours"
          value={operating_hours}
          onChange={(e) => setOperatingHours(e.target.value)}
          type="text"
        />

        <FormInput
          id="dropoff_instr"
          placeholder="Dropoff Instructions"
          labelText="Dropoff Instructions"
          value={dropoff_instr}
          onChange={(e) => setDropoffInstructions(e.target.value)}
          type="text"
        />

        <button type="submit" className="btn btn-primary">
          Update
        </button>
      </form>
      <p className="mt-5 alert alert-warning">
        fyi.. You're going be logged out after you hit Submit. Just Log In
        again, okay?
      </p>
    </div>
  );
}

export default ReceiverUpdate;
