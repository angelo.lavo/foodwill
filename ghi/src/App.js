import "./App.css";
import LoginForm from "./LoginForm";
import DonationNew from "./NewDonationForm.js";
import MainPage from "./MainPage.js";
import ProviderUpdate from "./ProviderUpdateForm.js";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from "./Nav";
import SignUp from "./SignupForm.js";
import { useGetTokenQuery } from "./store/userApi.js";
import VolunteerUpdate from "./volunteerUpdateForm.js";
import UserDonationsHistory from "./UserDonationsHistory.js";
import { DonationDetail } from "./DonationDetail.js";
import ProviderDashBoard from "./ProviderDash.js";
import VolunteerDashBoard from "./VolunteerDash.js";
import ReceiverDashBoard from "./ReceiverDashboard.js";
import ReceiverUpdate from "./ReceiverUpdateForm.js";

function App() {
  const { data: userData, isLoading } = useGetTokenQuery();

  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  if (userData) {
    function roleCheck() {
      if (userData.account.role === "provider") {
        return (
          <>
            <Route path="/">
              <Route
                path="dashboard"
                element={<ProviderDashBoard userData={userData} />}
              />
              <Route path="donations/new" element={<DonationNew />} />
              <Route
                path="provider/update"
                element={<ProviderUpdate userData={userData} />}
              />
              <Route path="donations/:id" element={<DonationDetail />}></Route>
              <Route
                path="donations/history"
                element={<UserDonationsHistory userData={userData} />}
              />
            </Route>
          </>
        );
      } else if (userData.account.role === "volunteer") {
        return (
          <>
            <Route
              path="dashboard"
              element={<VolunteerDashBoard userData={userData} />}
            />
            <Route
              path="volunteer/update"
              element={<VolunteerUpdate userData={userData} />}
            />
            <Route path="donations/:id" element={<DonationDetail />}></Route>
            <Route
              path="donations/history"
              element={<UserDonationsHistory userData={userData} />}
            />
          </>
        );
      } else {
        return (
          <>
            <Route
              path="dashboard"
              element={<ReceiverDashBoard userData={userData} />}
            />
            <Route
              path="receiver/update"
              element={<ReceiverUpdate userData={userData} />}
            />
            <Route path="donations/:id" element={<DonationDetail />}></Route>
            <Route
              path="donations/history"
              element={<UserDonationsHistory userData={userData} />}
            />
          </>
        );
      }
    }
    return (
      <BrowserRouter basename={basename}>
        <Nav />
        <Routes>
          <Route path="/">{roleCheck()}</Route>
        </Routes>
      </BrowserRouter>
    );
  } else {
    return (
      <BrowserRouter basename={basename}>
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />}></Route>
          <Route path="login" element={<LoginForm />}></Route>
          <Route path="signup" element={<SignUp />}></Route>
        </Routes>
      </BrowserRouter>
    );
  }
}
export default App;
