import React from "react";
import { useNavigate } from "react-router-dom";
import {
  useGetActiveDonationByUserQuery,
  useDeleteDonationMutation,
} from "./store/donationApi";

export default function ActiveProviderDonationsList(props) {
  const navigate = useNavigate();
  const { data: donationData, isLoading } = useGetActiveDonationByUserQuery(
    props.userData.account.id
  );
  const [deleteDonation] = useDeleteDonationMutation();

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  function handleDelete(id) {
    deleteDonation(id);
  }

  const getDate = (dateTime) => {
    const date = new Date(dateTime);
    return date.toDateString();
  };

  const getTime = (dateTime) => {
    const date = new Date(dateTime);
    return date.toLocaleTimeString();
  };

  function haveData() {
    if (donationData.donations.length > 0) {
      return (
        <>
          <table
            style={{ display: "block", height: "300px", overflowY: "scroll" }}
            className="table table-hover table-striped"
          >
            <thead>
              <tr>
                <th></th>
                <th>Status</th>
                <th>Quantity (lbs)</th>
                <th>Value</th>
                <th>Ready time</th>
              </tr>
            </thead>
            <tbody>
              {donationData.donations.map((donation) => {
                return (
                  <tr key={donation.id}>
                    <td>
                      <button
                        className="btn btn-outline-dark"
                        onClick={() => navigate(`/donations/${donation.id}`)}
                      >
                        {" "}
                        DETAILS{" "}
                      </button>
                      {donation.status === "UNCLAIMED" && (
                        <button
                          className="btn btn-outline-danger mt-2"
                          onClick={() => handleDelete(donation.id)}
                        >
                          {" "}
                          CANCEL{" "}
                        </button>
                      )}
                    </td>
                    <td>{donation.status}</td>
                    <td>{donation.quantity}</td>
                    <td>{donation.value}</td>
                    <td>
                      {getDate(donation.ready_time)}
                      <br /> {getTime(donation.ready_time)}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      );
    } else {
      return (
        <>
          <div className="alert alert-warning mt-5 fs-4" role="alert">
            You currently have no active donation.
          </div>
        </>
      );
    }
  }
  return (
    <>
      <div className="container text-center">
        <div
          style={{ height: "400px" }}
          className="row m-3 p-3 justify-content-center border border-secondary border-3 rounded"
        >
          <div className="col-6">
            <h3 className="text-muted">Your active donations</h3>
            {haveData()}
          </div>
        </div>
      </div>
    </>
  );
}
