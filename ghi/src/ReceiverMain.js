import React from "react";
import UnclaimedDonationsList from "./UnclaimedDonationsList";
import RecActiveDonations from "./RecActiveDonations";

export default function ReceiverMain(props) {
  return (
    <>
      <div>
        <UnclaimedDonationsList />
      </div>
      <div>
        <RecActiveDonations userData={props.userData} />
      </div>
    </>
  );
}
