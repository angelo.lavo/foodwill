import { useState } from "react";
import { useLogOutMutation, useUpdateUserMutation } from "./store/userApi";
import { useNavigate } from "react-router-dom";

function FormInput(props) {
  const { id, placeholder, labelText, value, onChange, type } = props;

  return (
    <div className="mb-3">
      <label htmlFor={id} className="form-label">
        {labelText}
      </label>
      <input
        value={value}
        onChange={onChange}
        type={type}
        className="form-control"
        id={id}
        placeholder={placeholder}
      />
    </div>
  );
}

function VolunteerUpdate(props) {
  const navigate = useNavigate();
  const [updateUser] = useUpdateUserMutation();
  const [logOut] = useLogOutMutation();
  const [name, setName] = useState(props.userData.account.full_name);
  const [address, setAddress] = useState(props.userData.account.address);
  const [phone_number, setPhoneNumber] = useState(
    props.userData.account.phone_number
  );
  const [plate_num, setLicensePlateNumber] = useState(
    props.userData.account.plate_num
  );

  const handleSubmit = (e) => {
    e.preventDefault();
    let data = {
      full_name: name,
      address: address,
      phone_number: phone_number,
      plate_num: plate_num,
    };
    updateUser(data)
      .unwrap()
      .then((payload) => {
        logOut()
          .unwrap()
          .then((payload) => {
            navigate("/login");
          })
          .catch((error) => console.error("rejected", error));
      })
      .catch((error) => console.error("rejected", error));
  };

  return (
    <div className="container">
      <div className="alert d-none" role="alert" id="submitted">
        <h5 className="alert-heading">Changes submitted</h5>
      </div>
      <form onSubmit={handleSubmit} id="form">
        <FormInput
          id="name"
          placeholder="Full name"
          labelText="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          type="text"
        />
        <FormInput
          id="address"
          placeholder="Address"
          labelText="Address"
          value={address}
          onChange={(e) => setAddress(e.target.value)}
          type="text"
        />
        <FormInput
          id="phone_number"
          placeholder="Phone number"
          labelText="Phone number"
          value={phone_number}
          onChange={(e) => setPhoneNumber(e.target.value)}
          type="text"
        />
        <FormInput
          id="plate_num"
          placeholder="License plate number"
          labelText="License plate number"
          value={plate_num}
          onChange={(e) => setLicensePlateNumber(e.target.value)}
          type="text"
        />
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
      <p className="mt-5 alert alert-warning">
        fyi.. You're going be logged out after you hit Submit. Just Log In
        again, okay?
      </p>
    </div>
  );
}
export default VolunteerUpdate;
