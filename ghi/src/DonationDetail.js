import React from "react";
import { useGetDonationDetailQuery } from "./store/donationApi";
import { useParams } from "react-router-dom";

export const DonationDetail = () => {
  const params = useParams();
  const { data, isFetching, isSuccess } = useGetDonationDetailQuery(params.id);

  const getDateTime = (dateTime) => {
    const date = new Date(dateTime);
    return date.toLocaleString();
  };

  let content;

  if (isFetching) {
    content = <p>Is Loading</p>;
  } else if (isSuccess) {
    content = (
      <>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-8">
              <div
                className="card shadow mb-3 mt-5 p-3"
                style={{ maxWidth: "800px" }}
              >
                <div className="row g-0">
                  <div className="col-md-4">
                    <img
                      src="/Foodwill-vertical.png"
                      className="img-fluid rounded-start"
                      alt="..."
                    />
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title alert alert-success text-center">
                        {data.status}
                      </h5>
                      <ul className="list-group list-group-flush">
                        <li className="list-group-item">
                          Active: {String(data.active)}
                        </li>
                        <li className="list-group-item">
                          Quantity: {data.quantity} lbs
                        </li>
                        <li className="list-group-item">Value: ${data.value}</li>
                        <li className="list-group-item">
                          Ready Time: {getDateTime(data.ready_time)}
                        </li>
                        <li className="list-group-item">
                          Provider:
                          <br /> ------Name: {data.prov_name}
                          <br /> ------Address: {data.prov_address}
                          <br /> ------Phone Number: {data.prov_phone}
                          <br /> ------Pickup Instructions:{" "}
                          {data.pickup_instructions}
                        </li>
                        <li className="list-group-item">
                          Receiver:
                          <br /> ------Name: {data.rec_name}
                          <br /> ------Address: {data.rec_address}
                          <br /> ------Phone Number: {data.rec_phone}
                          <br /> ------Dropoff Instructions:{" "}
                          {data.dropoff_instructions}
                        </li>
                        <li className="list-group-item">
                          Courier:
                          <br /> ------Name: {data.vol_name}
                          <br /> ------Phone Number: {data.vol_phone}
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
  return <section>{content}</section>;
};
