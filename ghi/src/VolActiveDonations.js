import React from "react";
import { useNavigate } from "react-router-dom";
import {
  useGetActiveDonationByUserQuery,
  useUpdateDonationMutation,
} from "./store/donationApi";

export default function VolActiveDonations(props) {
  const navigate = useNavigate();
  const { data: donationData, isLoading } = useGetActiveDonationByUserQuery(
    props.userData.account.id
  );
  const [updateDonation] = useUpdateDonationMutation();

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  function handleComplete(id) {
    let payload = {
      status: "COMPLETED",
      active: false,
    };
    let body = {
      data: payload,
      id: id,
    };
    updateDonation(body);
  }

  function handleCancel(id) {
    let payload = {
      status: "CLAIMED",
      volunteer_id: "",
      vol_name: "",
      vol_phone: "",
    };
    let body = {
      data: payload,
      id: id,
    };
    updateDonation(body);
  }

  const getDate = (dateTime) => {
    const date = new Date(dateTime);
    return date.toDateString();
  };

  const getTime = (dateTime) => {
    const date = new Date(dateTime);
    return date.toLocaleTimeString();
  };

  function haveData() {
    if (donationData.donations.length > 0) {
      return (
        <table
          style={{ display: "block", height: "300px", overflowY: "scroll" }}
          className="table table-hover table-striped "
        >
          <thead>
            <tr>
              <th></th>
              <th>Status</th>
              <th>From</th>
              <th>To</th>
              <th>Ready time</th>
            </tr>
          </thead>
          <tbody>
            {donationData.donations.map((donation) => {
              return (
                <tr key={donation.id}>
                  <td>
                    <button
                      className="btn btn-outline-success"
                      onClick={() => handleComplete(donation.id)}
                    >
                      {" "}
                      COMPLETE{" "}
                    </button>
                    <button
                      className="btn btn-outline-danger mt-2 w-100"
                      onClick={() => handleCancel(donation.id)}
                    >
                      {" "}
                      CANCEL
                      <br /> DELIVERY{" "}
                    </button>
                    <button
                      className="btn btn-outline-dark w-100 mt-2"
                      onClick={() => navigate(`/donations/${donation.id}`)}
                    >
                      {" "}
                      DETAILS{" "}
                    </button>
                  </td>
                  <td>{donation.status}</td>
                  <td>
                    {donation.prov_name}
                    <br /> {donation.prov_address}
                    <br /> {donation.prov_phone}
                  </td>
                  <td>
                    {donation.rec_name}
                    <br /> {donation.rec_address}
                    <br /> {donation.rec_phone}
                  </td>
                  <td>
                    {getDate(donation.ready_time)}
                    <br /> {getTime(donation.ready_time)}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    } else {
      return (
        <>
          <div className="alert alert-warning mt-5 fs-4" role="alert">
            You currently have no active donation.
          </div>
        </>
      );
    }
  }

  return (
    <>
      <div className="container text-center">
        <div
          style={{ height: "400px" }}
          className="row m-3 p-3 justify-content-center border border-secondary border-3 rounded"
        >
          <div className="col-6">
            <h3 className="text-muted">Your active donations</h3>
            {haveData()}
          </div>
        </div>
      </div>
    </>
  );
}
