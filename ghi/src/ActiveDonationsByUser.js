import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  useGetActiveDonationByUserQuery,
  useUpdateDonationMutation,
} from "./store/donationApi";
import { useGetTokenQuery } from "./store/userApi";

export const ActiveUserDonationsList = () => {
  const params = useParams();
  const navigate = useNavigate();
  const { data: donationData, isLoading } = useGetActiveDonationByUserQuery(
    params.user_id
  );
  const { data: userData } = useGetTokenQuery();
  const [updateDonation] = useUpdateDonationMutation();

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  function handleClaim(id) {
    let payload = {
      status: "ENROUTE",
      volunteer_id: userData.account.id,
    };
    let body = {
      data: payload,
      id: id,
    };
    updateDonation(body);
  }

  const seeDetails = async (id) => {
    navigate(`/donations/${id}`);
  };

  const getDateTime = (dateTime) => {
    const date = new Date(dateTime);
    return date.toLocaleString();
  };

  function handleDeliver(id) {
    let payload = {
      status: "ENROUTE",
      receiver_id: userData.account.id,
    };
    let body = {
      data: payload,
      id: id,
    };
    updateDonation(body);
  }

  if (!userData) {
    return <h2>Please Log In for more functions</h2>;
  }

  return (
    <>
      <div className="container">
        <div style={{ height: "400px" }} className="row justify-content-center">
          <div className="col-6">
            <h3>Donations to deliver</h3>
            <table
              style={{ display: "block", height: "300px", overflowY: "scroll" }}
              className="table table-hover table-striped "
            >
              <thead>
                <tr>
                  <th></th>
                  {/* <th>ID</th> */}
                  {/* <th>Active</th> */}
                  <th>Status</th>
                  <th>Quantity</th>
                  <th>Value</th>
                  {/* <th>Deli Instruc</th> */}
                  <th>Ready time</th>
                  {/* <th>volun id</th>
                  <th>provider id</th>
                  <th>receiver id</th> */}
                </tr>
              </thead>
              <tbody>
                {donationData.donations.map((donation) => {
                  return (
                    <tr key={donation.id}>
                      <td>
                        <button
                          className="btn btn-success"
                          onClick={() => handleDeliver(donation.id)}
                        >
                          {" "}
                          DELIVER{" "}
                        </button>
                      </td>
                      {/* <td>{donation.id}</td>
                      <td>{String(donation.active)}</td> */}
                      <td>{donation.status}</td>
                      <td>{donation.quantity}</td>
                      <td>{donation.value}</td>
                      {/* <td>{donation.delivery_instructions}</td> */}
                      <td>{getDateTime(donation.ready_time)}</td>
                      {/* <td>{donation.volunteer_id}</td>
                      <td>{donation.provider_id}</td>
                      <td>{donation.receiver_id}</td> */}
                    </tr>
                  );
                })}
              </tbody>
            </table>
            {/* <div>
              <Outlet />
            </div> */}
          </div>
        </div>
      </div>
    </>
  );
};
