import React from "react";
import {
  useGetDonationByStatusQuery,
  useUpdateDonationMutation,
} from "./store/donationApi";
import { useGetTokenQuery } from "./store/userApi";

export default function ClaimedDonationsList() {
  const { data: donationData, isLoading } =
    useGetDonationByStatusQuery("CLAIMED");
  const { data: userData } = useGetTokenQuery();
  const [updateDonation] = useUpdateDonationMutation();

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  function handleDeliver(id) {
    let payload = {
      status: "ENROUTE",
      volunteer_id: userData.account.id,
      vol_name: userData.account.full_name,
      vol_phone: userData.account.phone_number,
    };
    let body = {
      data: payload,
      id: id,
    };
    updateDonation(body);
  }

  const getDate = (dateTime) => {
    const date = new Date(dateTime);
    return date.toDateString();
  };

  const getTime = (dateTime) => {
    const date = new Date(dateTime);
    return date.toLocaleTimeString();
  };

  function haveData() {
    if (donationData.donations.length > 0) {
      return (
        <table
          style={{ display: "block", height: "300px", overflowY: "scroll" }}
          className="table table-hover table-striped "
        >
          <thead>
            <tr>
              <th></th>
              <th>Status</th>
              <th>From</th>
              <th>To</th>
              <th>Ready time</th>
            </tr>
          </thead>
          <tbody>
            {donationData.donations.map((donation) => {
              return (
                <tr key={donation.id}>
                  <td>
                    <button
                      className="btn btn-outline-success"
                      onClick={() => handleDeliver(donation.id)}
                    >
                      {" "}
                      DELIVER{" "}
                    </button>
                  </td>
                  <td>{donation.status}</td>
                  <td>
                    {donation.prov_name}
                    <br /> {donation.prov_address}
                    <br /> {donation.prov_phone}
                  </td>
                  <td>
                    {donation.rec_name}
                    <br /> {donation.rec_address}
                    <br /> {donation.rec_phone}
                  </td>
                  <td>
                    {getDate(donation.ready_time)}
                    <br /> {getTime(donation.ready_time)}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    } else {
      return (
        <>
          <div className="alert alert-warning mt-5 fs-4" role="alert">
            Sorry, there's no donation available right now.
          </div>
        </>
      );
    }
  }

  return (
    <>
      <div className="container text-center">
        <div
          style={{ height: "400px" }}
          className="row m-3 p-3 justify-content-center border border-secondary border-3 rounded "
        >
          <div className="col-6">
            <h3 className="text-muted">Donations ready for delivery</h3>
            {haveData()}
          </div>
        </div>
      </div>
    </>
  );
}
