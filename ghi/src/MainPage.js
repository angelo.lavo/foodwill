import React from "react";
import { useNavigate } from "react-router-dom";

function MainPage() {
  const navigate = useNavigate();

  return (
    <>
      <div className="text-center">
        <img src="Foodwill-cropped.png" alt="..."></img>
      </div>
      <div className="d-grid gap-2 col-2 mx-auto">
        <button
          className="btn btn-outline-dark"
          onClick={() => {
            navigate("/login");
          }}
          type="button"
        >
          LOG IN
        </button>
        <button
          className="btn btn-outline-dark"
          onClick={() => {
            navigate("/signup");
          }}
          type="button"
        >
          SIGN UP
        </button>
      </div>
      <div className="mt-5 text-center font-monospace text-muted">
        <p style={{ fontSize: "14px" }}>Copyright © Los Indefinidos 2022</p>
      </div>
    </>
  );
}
export default MainPage;
