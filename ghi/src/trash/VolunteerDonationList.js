import React from "react";
import { useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { DonationDetail } from "./DonationDetail";
import {
  // useGetVolunteerDonationsQuery,
  useUpdateDonationMutation,
} from "./store/donationApi";
import { useGetTokenQuery } from "./store/userApi";

export default function VolunteerDonationsList() {
  const navigate = useNavigate();
  const {
    data: donationData,
    error,
    isLoading,
  } = useGetVolunteerDonationsQuery();
  const { data: userData } = useGetTokenQuery();
  const [updateDonation, result] = useUpdateDonationMutation();

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  function handleClaim(id) {
    let payload = {
      status: "ENROUTE",
      volunteer_id: userData.account.id,
    };
    let body = {
      data: payload,
      id: id,
    };
    updateDonation(body);
  }

  const seeDetails = async (id) => {
    // e.preventDefault();
    navigate(`/donation/${id}`);
    // navigate(`/list`)

    // navigate({DonationDetail})
  };

  const getDateTime = (dateTime) => {
    const date = new Date(dateTime);
    return date.toLocaleString();
  };

  if (!userData) {
    return <h2>Please Log In for more functions</h2>;
  }

  return (
    <>
      <h1>Donations Lists</h1>
      <table className="table table-hover table-striped">
        <thead>
          <tr>
            <th></th>
            <th>ID</th>
            <th>Status</th>
            <th>Quantity</th>
            <th>Value</th>
            <th>Delivery Instructions</th>
            <th>Ready for pickup</th>
            <th>volunteer id</th>
            <th>provider id</th>
            <th>receiver id</th>
          </tr>
        </thead>
        <tbody>
          {donationData.donations.map((donation) => {
            return (
              <tr key={donation.id}>
                <td>
                  <button
                    className="btn btn-success"
                    onClick={() => handleClaim(donation.id)}
                  >
                    {" "}
                    DELIVER{" "}
                  </button>
                  <button
                    className="btn btn-success"
                    onClick={() => seeDetails(donation.id)}
                  >
                    {" "}
                    DETAILS{" "}
                  </button>
                </td>
                <td>{donation.id}</td>
                <td>{donation.status}</td>
                <td>{donation.quantity}</td>
                <td>{donation.value}</td>
                <td>{donation.delivery_instructions}</td>
                <td>{getDateTime(donation.ready_time)}</td>
                <td>{donation.volunteer_id}</td>
                <td>{donation.provider_id}</td>
                <td>{donation.receiver_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
