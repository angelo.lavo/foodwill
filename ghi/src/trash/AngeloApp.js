import { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AllDonationsList from "./AllDonationsList";
import MainPage from "./MainPage";
import VolunteerUpdate from "./volunteerUpdateForm";
import LoginForm from "./LoginForm";
import DonationsList from "./TestDonations.js";
import DonationNew from "./TestDonationNew.js";
import { useGetTokenQuery } from "./store/userApi";
import Nav from "./TonyNav";
import { DonationDetail } from "./DonationDetail";
// import VolunteerDonationsList from './VolunteerDonationList';
import { useGetDonationQuery } from "./store/donationApi";
import { UserDonationsList } from "./TestUserDonationsList";
import { ActiveUserDonationsList } from "./ActiveDonationsByUser";
// import Construct from './Construct.js'
// import ErrorNotification from './ErrorNotification';
// import './App.css';

function App() {
  const { data: userData, isLoading } = useGetTokenQuery();
  // const { data: donationData } = useGetDonationQuery();
  // const [defaultUserData, setDefaultUserData] = useState('');

  if (userData) {
    return (
      <BrowserRouter>
        <div>
          <Routes>
            <Route path="/" element={<MainPage />} />
            {/* <Route path="volunteerdonations" element={<VolunteerDonationsList />} /> */}
            <Route
              path="volunteerupdate"
              element={<VolunteerUpdate userData={userData} />}
            ></Route>
            <Route path="login" element={<LoginForm />}></Route>
            <Route path="list" element={<DonationsList />}></Route>
            <Route path="new" element={<DonationNew />}></Route>
            <Route path="donations/:id" element={<DonationDetail />}></Route>
            <Route
              path="donations/user/:user_id"
              element={<UserDonationsList />}
            ></Route>
            <Route
              path="donations/active/user/:user_id"
              element={<ActiveUserDonationsList />}
            ></Route>
          </Routes>
        </div>
      </BrowserRouter>
    );
  }
  return <h1>Still loading, try logging in</h1>;
}

export default App;
