import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { userApi } from "./userApi";

export const donationApi = createApi({
  reducerPath: "donations",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_DONATION_SERVICE_API_HOST,
    prepareHeaders: (headers, { getState }) => {
      const selector = userApi.endpoints.getToken.select();
      const { data: tokenData } = selector(getState());
      if (tokenData && tokenData.access_token) {
        headers.set("Authorization", `Bearer ${tokenData.access_token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["DonationList"],

  endpoints: (builder) => ({
    getDonations: builder.query({
      query: () => ({
        url: "/api/donations/",
        credentials: "include",
      }),
      providesTags: ["DonationList"],
    }),

    getDonationByStatus: builder.query({
      query: (status) => ({
        url: `/api/donations/?status=${status}`,
        credentials: "include",
      }),
      providesTags: ["DonationList"],
    }),

    getDonationByUser: builder.query({
      query: (user_id) => ({
        url: `/api/donations/user/${user_id}`,
        credentials: "include",
      }),
      providesTags: ["DonationList"],
    }),

    getActiveDonationByUser: builder.query({
      query: (user_id) => ({
        url: `/api/donations/active/user/${user_id}`,
        credentials: "include",
      }),
      providesTags: ["DonationList"],
    }),

    getDonationDetail: builder.query({
      query: (id) => ({
        url: `/api/donations/${id}`,
        credentials: "include",
      }),
    }),

    createDonation: builder.mutation({
      query: (data) => ({
        url: "/api/donations/",
        method: "post",
        credentials: "include",
        body: data,
      }),
      invalidatesTags: ["DonationList"],
    }),

    deleteDonation: builder.mutation({
      query: (id) => ({
        url: `/api/donations/${id}`,
        method: "delete",
        credentials: "include",
      }),
      invalidatesTags: ["DonationList"],
    }),

    updateDonation: builder.mutation({
      query: ({ data, id }) => ({
        url: `/api/donations/${id}`,
        method: "put",
        credentials: "include",
        body: data,
      }),
      invalidatesTags: ["DonationList"],
    }),
  }),
});

export const {
  useGetDonationsQuery,
  useCreateDonationMutation,
  useUpdateDonationMutation,
  useGetDonationDetailQuery,
  useGetDonationByStatusQuery,
  useGetDonationByUserQuery,
  useGetActiveDonationByUserQuery,
  useDeleteDonationMutation,
} = donationApi;
