import React from "react";
import { useNavigate } from "react-router-dom";
import {
  useGetActiveDonationByUserQuery,
  useUpdateDonationMutation,
} from "./store/donationApi";

export default function RecActiveDonations(props) {
  const navigate = useNavigate();
  const { data: donationData, isLoading } = useGetActiveDonationByUserQuery(
    props.userData.account.id
  );

  const [updateDonation] = useUpdateDonationMutation();

  if (isLoading) {
    return <progress className="progress is-primary" max="100"></progress>;
  }

  function handleUnclaim(id) {
    let payload = {
      status: "UNCLAIMED",
      receiver_id: "",
      rec_name: "",
      rec_phone: "",
      rec_address: "",
      dropoff_instructions: "",
    };
    let body = {
      data: payload,
      id: id,
    };
    updateDonation(body);
  }

  function haveData() {
    if (donationData.donations.length > 0) {
      return (
        <table
          style={{ display: "block", height: "300px", overflowY: "scroll" }}
          className="table table-hover table-striped "
        >
          <thead>
            <tr>
              <th></th>
              <th>Status</th>
              <th>From</th>
              <th>Courier</th>
            </tr>
          </thead>
          <tbody>
            {donationData.donations.map((donation) => {
              return (
                <tr key={donation.id}>
                  <td>
                    <button
                      className="btn btn-outline-danger w-100"
                      onClick={() => handleUnclaim(donation.id)}
                    >
                      {" "}
                      UNCLAIM{" "}
                    </button>
                    <button
                      className="btn btn-outline-dark w-100 mt-2"
                      onClick={() => navigate(`/donations/${donation.id}`)}
                    >
                      {" "}
                      DETAILS{" "}
                    </button>
                  </td>
                  <td>{donation.status}</td>
                  <td>
                    {donation.prov_name}
                    <br /> {donation.prov_phone}
                  </td>
                  <td>
                    {donation.vol_name}
                    <br /> {donation.vol_phone}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    } else {
      return (
        <>
          <div className="alert alert-warning mt-5 fs-4" role="alert">
            You currently have no active donation.
          </div>
        </>
      );
    }
  }

  return (
    <>
      <div className="container text-center">
        <div
          style={{ height: "400px" }}
          className="row m-3 p-3 justify-content-center border border-secondary border-3 rounded"
        >
          <div className="col-6">
            <h3 className="text-muted">Your active donations</h3>
            {haveData()}
          </div>
        </div>
      </div>
    </>
  );
}
